var app = getApp();
const db = wx.cloud.database();
const _ = db.command;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    scrollTop: 0,
    newlist: [],
    list: [],
    key: '',
    blank: false,
    hislist: [],
  },
  onLoad: function(options) {
    this.gethis();
    this.getnew();
},
 //获取本地记录
 gethis() {
  let that = this;
  wx.getStorage({
        key: 'history',
        success: function(res) {
              let hislist = JSON.parse(res.data);
              //限制长度
              if (hislist.length > 5) {
                    hislist.length = 5
              }
              that.setData({
                    hislist: hislist
              })
        },
  })
},
//选择历史搜索关键词
choosekey(e) {
  this.data.key = e.currentTarget.dataset.key;
  this.search('his');
},
//最新推荐商品
getnew() {
  let that = this;
  db.collection('lost').where({
      createTime: _.lt(new Date().getTime())
  }).orderBy('_id', 'desc').get({
        success: function(res) {
              let newlist = res.data;
              //限定5个推荐内容
              if (newlist.length > 5) {
                    newlist.length = 5;
              }
              that.setData({
                    newlist: newlist,
              })
        }
  })
},
 //搜索结果
 search(n) {
  let that = this;
  let key = that.data.key;
  if (key == '') {
        wx.showToast({
              title: '请输入关键词',
              icon: 'none',
        })
        return false;
  }
  wx.setNavigationBarTitle({
        title:'"'+ that.data.key + '"的搜索结果',
  })
  wx.showLoading({
        title: '加载中',
  })
  if (n !== 'his') {
        that.history(key);
  }
  db.collection('lost').where({
        createTime: _.lt(new Date().getTime()),
        releaseText: db.RegExp({
            regexp: '.*' + key + '.*',
            options: 'i',})
 }).orderBy('_id', 'desc').limit(20).get({
        success(e) {
              wx.hideLoading();
              that.setData({
                    blank: true,
                    page: 0,
                    list: e.data,
              })
        }
  })
},
    
 //添加到搜索历史
 history(key) {
  let that = this;
  wx.getStorage({
        key: 'history',
        success(res) {
              let oldarr = JSON.parse(res.data); //字符串转数组
              let newa = [key]; //对象转为数组
              let newarr = JSON.stringify(newa.concat(oldarr)); //连接数组\转字符串
              wx.setStorage({
                    key: 'history',
                    data: newarr,
              })
        },
        fail(res) {
              //第一次打开时获取为null
              let newa = [key]; //对象转为数组
              var newarr = JSON.stringify(newa); //数组转字符串
              wx.setStorage({
                    key: 'history',
                    data: newarr,
              })
        }
  });
},
keyInput(e) {
  this.data.key = e.detail.value
},
//至顶
gotop() {
  wx.pageScrollTo({
        scrollTop: 0
  })
},
//监测屏幕滚动
onPageScroll: function (e) {
  this.setData({
        scrollTop: parseInt((e.scrollTop) * wx.getSystemInfoSync().pixelRatio)
  })
},
})